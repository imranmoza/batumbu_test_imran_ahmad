#proccess create table m_item
create table m_item(
item_id bigint not null auto_increment,
item_name varchar(100) not null,
item_desc varchar(100) null,
item_unit varchar(10) null,
item_price decimal(18.0) null default 0,
item_created_by int not null,
item_created_on timestamp not null default current_timestamp,
item_modified_by int null,
item_modified_on timestamp default null on update current_timestamp,
item_isactive boolean default true,
item_isdelete boolean default false,
primary key (item_id)
);

#proccess insert data master item
insert into m_item(item_name, item_desc, item_unit, item_price, item_created_by)
values('indomie goreng', 'indomie selera indonesia', 'pcs', 3000, 1),
		('indomie ayam bawang', 'indomie selera indonesia', 'pcs', 2400, 1),
        ('Vit C500', 'Vitamin C 1000', 'pcs', 1500, 1),
        ('Susu Beruang', 'Susu Steril', 'pcs', 9700, 1);
        
#proccess GET Item
select * from m_item;