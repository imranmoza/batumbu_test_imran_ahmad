
create table tr_transaction(
trans_id bigint not null auto_increment,
trans_cust_id bigint not null comment 'foreign key to table m_customer',
trans_total_item int not null default 0,
trans_total_qty_item int null default 0,
trans_total_amount decimal(18.0) null default 0,
trans_payment_type int comment 'foreign key to table m_payment_type',
trans_payment_status_id int null comment'foreign key to m_payment_status',
trans_status_id int comment 'foreign key to table m_transaction_status',
trans_created_by bigint not null comment 'foreign key to table m_customer',
trans_created_on timestamp default current_timestamp,
trans_modified_by bigint null,
trans_modified_on timestamp default null on update current_timestamp,
trans_isactive boolean not null default true,
trans_isdelete boolean not null default false,
primary key (trans_id)
);

create table tr_transaction_detail(
tr_detail_id bigint not null auto_increment,
tr_detail_trans_id bigint not null comment 'foreignkey to table tr_transaction',
tr_detail_item_id bigint comment 'foreign key to table m_item',
tr_detail_qty int, 
tr_detail_total_amount_item decimal(18.0) default 0,
tr_detail_isactive boolean default true,
tr_detail_isdelete boolean default false,
primary key (tr_detail_id)
);

#process insert 
insert into tr_transaction(trans_cust_id, trans_total_item, trans_total_qty_item, trans_total_amount, trans_created_by)
values(2, 2, 3, 0, 2);

insert into tr_transaction_detail(tr_detail_trans_id, tr_detail_item_id, tr_detail_qty, tr_detail_total_amount_item)
select 1, 4, 1, (1 * item_price) from m_item where item_id = 4;


#proses set total qty, total amount table tr_transaction
update tr_transaction a
inner join (
			select a.trans_id, count(tr_detail_id) as total_item, 
					sum(tr_detail_qty) as total_qty,
					sum(tr_detail_total_amount_item) as total_amount
			from  tr_transaction a 
			inner join tr_transaction_detail b on b.tr_detail_trans_id = a.trans_id
			group by a.trans_id) b on a.trans_id = b.trans_id
set a.trans_total_item = b.total_item,
	a.trans_total_qty_item = b.total_qty,
    a.trans_total_amount = b.total_amount;


select * from tr_transaction;
select * from tr_transaction_detail;

#transaction report
select tr.trans_id, concat(c.cust_first_name, c.cust_last_name) as cust_name, #tr.trans_total_item, tr.trans_total_qty_item,
	tr.trans_total_amount,
	i.item_name, i.item_price, td.tr_detail_qty, td.tr_detail_total_amount_item 
from tr_transaction tr
left join m_customer c on c.cust_id = tr.trans_cust_id
inner join tr_transaction_detail td on td.tr_detail_trans_id = tr.trans_id
left join m_item i on i.item_id = td.tr_detail_item_id ;