#createDB
create database batumbu;

#create table 
create table m_customer(
cust_id bigint not null auto_increment,
cust_first_name varchar(100) null,
cust_last_name varchar(100) null,
cust_created_by int not null,
cust_created_on timestamp not null default current_timestamp,
cust_modified_by int null,
cust_modified_on timestamp default null on update current_timestamp,
cust_isactive boolean not null default true,
cust_isdelete boolean not null default false,
primary key (cust_id)
);

#proccess insert 
insert into m_customer(cust_first_name, cust_last_name, cust_created_by)
values('Imran', 'Ahmad', 1), ('Hutri', 'Agnery', 1), ('Rahayu Poza', 'Aulia', 1), ('Muharsyad', 'Al-Azib', 1);

#proccess Get
select * from m_customer;